from django.db import models


def filter_active_objects(queryset):
    return queryset.filter(deleted_at__isnull=True)


class SoftDeletedManager(models.Manager):

    def get_queryset(self):
        queryset = super().get_queryset()
        return filter_active_objects(queryset)
