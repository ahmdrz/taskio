from django.contrib import admin


class FilteredActiveObjectsModelAdmin(admin.ModelAdmin):

    def get_queryset(self, request):
        from utilities.managers import filter_active_objects
        return filter_active_objects(super().get_queryset(request))
