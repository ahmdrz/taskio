from django.db import models
from django.utils import timezone

from .managers import SoftDeletedManager


class SoftDeleteModel(models.Model):
    active_objects = SoftDeletedManager()
    objects = models.Manager()
    deleted_at = models.DateTimeField(
        null=True,
        db_index=True,
        default=None,
    )

    def soft_delete(self, using=None, save=True):
        self.deleted_at = timezone.now()
        if save:
            self.save(using=using, update_fields=['deleted_at'])

    def delete(self, *args, **kwargs):
        if 'soft' in kwargs and kwargs['soft']:
            self.soft_delete(using=kwargs.get('using'), save=True)
        else:
            return super().delete(*args, **kwargs)

    def restore(self, save=True):
        self.deleted_at = None
        if save:
            self.save(update_fields=['deleted_at'])

    class Meta:
        abstract = True


class TimeStampedModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, db_index=True)
    modified_at = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        update_fields = kwargs.get('update_fields', None)
        if update_fields:
            kwargs['update_fields'] = set(update_fields).union({'modified_at'})
        super().save(*args, **kwargs)

    class Meta:
        abstract = True
