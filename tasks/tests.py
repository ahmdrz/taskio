from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from tasks.models import Task


class TestCaseBase(TestCase):
    tasks_url = reverse('tasks_view')

    def setUp(self):
        data = {'username': 'jacob', 'password': 'hello-world'}
        self.user = get_user_model().objects.create_user(**data)
        auth_url = reverse('token_obtain_pair')
        resp = self.client.post(auth_url, data, format='json')
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.access_token = resp.data['access']
        self.refresh_token = resp.data['refresh']
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.access_token)

    def get_task_url(self, pk):
        return reverse('task_view', kwargs={'pk': pk})

    def create(self, title):
        resp = self.client.post(self.tasks_url, {'title': title}, format='json')
        self.assertEqual(resp.status_code, status.HTTP_201_CREATED)
        self.assertTrue(Task.active_objects.exists())
        instance = Task.active_objects.filter(id=resp.data['id']).first()
        self.assertIsNotNone(instance)
        return instance


class TestTaskCRUD(TestCaseBase):

    def test_list(self):
        pet_task = self.create(title='PET test')
        mri_task = self.create(title='MRI test')
        resp = self.client.get(self.tasks_url, format='json')
        self.assertEqual(resp.data['count'], Task.active_objects.count())
        results = resp.data['results']
        self.assertEqual(results[0]['id'], pet_task.id)
        self.assertEqual(results[1]['id'], mri_task.id)
        self.assertTrue(results[0]['created_at'] < results[1]['created_at'])

        results = self.client.get(self.tasks_url, data={'ordering': '-created_at'}, format='json').data['results']
        self.assertTrue(results[0]['created_at'] > results[1]['created_at'])

        results = self.client.get(self.tasks_url, data={'search': 'MRI'}, format='json').data['results']
        self.assertEqual(results[0]['id'], mri_task.id)

    def test_retrieve(self):
        pet_task = self.create(title='PET test')
        task_url = self.get_task_url(pet_task.id)
        resp = self.client.get(task_url, format='json')
        self.assertTrue(resp.data['id'], pet_task.id)

    def test_update(self):
        pet_task = self.create(title='PET test')
        task_url = self.get_task_url(pet_task.id)
        new_title = 'PET task'
        resp = self.client.put(task_url, data={'title': new_title}, format='json')
        self.assertEqual(resp.data['id'], pet_task.id)
        self.assertEqual(resp.data['title'], new_title)

    def test_delete(self):
        mri_reminder = self.create(title='MRI reminder')
        task_url = self.get_task_url(mri_reminder.id)
        resp = self.client.delete(task_url)
        self.assertEqual(resp.status_code, status.HTTP_204_NO_CONTENT)
        resp = self.client.get(task_url, format='json')
        self.assertEqual(resp.status_code, status.HTTP_404_NOT_FOUND)


class TestTaskStatus(TestCaseBase):

    def test_task_status(self):
        task = self.create('MRI test')
        url = reverse('task_mark_view', kwargs={'pk': task.id})
        result = self.client.get(url, format='json').json()
        self.assertIsNotNone(result['completed_at'])

        task.refresh_from_db()
        self.assertTrue(task.is_completed)

        url = reverse('task_unmark_view', kwargs={'pk': task.id})
        result = self.client.get(url, format='json').json()
        self.assertIsNone(result['completed_at'])

        task.refresh_from_db()
        self.assertFalse(task.is_completed)
