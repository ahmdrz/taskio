from django.conf import settings
from django.db import models

from utilities.models import TimeStampedModel, SoftDeleteModel


class Task(SoftDeleteModel, TimeStampedModel):
    title = models.CharField(max_length=64)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    completed_at = models.DateTimeField(
        null=True,
        blank=True,
    )

    @property
    def is_completed(self):
        return self.completed_at is not None

    def __str__(self):
        return self.title
