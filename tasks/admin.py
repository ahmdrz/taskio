from django.contrib import admin

from tasks.models import Task
from utilities.admin import FilteredActiveObjectsModelAdmin


@admin.register(Task)
class TaskAdmin(FilteredActiveObjectsModelAdmin):
    list_display = ('id', 'title')
    search_fields = ('title',)
    fields = (
        'title',
        'modified_at',
        'created_at',
        'user',
    )
    autocomplete_fields = ('user',)
    readonly_fields = ('created_at', 'modified_at')
