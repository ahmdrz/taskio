from django.http import Http404, JsonResponse
from django.utils import timezone
from rest_framework import viewsets, filters, status
from rest_framework.views import APIView

from .enums import TASK_MARK_OPERATION
from .models import Task
from .serializers import TaskSerializer


class TasksViewSet(viewsets.ModelViewSet):
    serializer_class = TaskSerializer
    queryset = Task.active_objects.all()
    filter_backends = [
        filters.SearchFilter,
        filters.OrderingFilter,
    ]
    search_fields = ['title']
    ordering_fields = ['created_at', 'modified_at']

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class TaskStatusControllerAPIView(APIView):
    def get(self, request, *args, **kwargs):
        instance = Task.active_objects.filter(id=kwargs.get('pk')).first()
        if not instance:
            raise Http404
        operation = kwargs.get('operation')
        instance.completed_at = timezone.now() if operation == TASK_MARK_OPERATION else None
        instance.save(update_fields=['completed_at'])
        return JsonResponse(
            data=TaskSerializer(instance=instance).data,
            status=status.HTTP_200_OK,
        )
