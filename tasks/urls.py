from django.urls import path

from .enums import TASK_MARK_OPERATION, TASK_UNMARK_OPERATION
from .views import TasksViewSet, TaskStatusControllerAPIView

urlpatterns = [
    path('', TasksViewSet.as_view({
        'get': 'list',
        'post': 'create',
    }), name='tasks_view'),
    path('<int:pk>/', TasksViewSet.as_view({
        'get': 'retrieve',
        'put': 'update',
        'delete': 'destroy',
    }), name='task_view'),
    path(
        '<int:pk>/status/mark/',
        TaskStatusControllerAPIView.as_view(),
        name='task_mark_view',
        kwargs={'operation': TASK_MARK_OPERATION},
    ),
    path(
        '<int:pk>/status/unmark/',
        TaskStatusControllerAPIView.as_view(),
        name='task_unmark_view',
        kwargs={'operation': TASK_UNMARK_OPERATION},
    ),
]
