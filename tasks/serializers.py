from rest_framework import serializers

from tasks.models import Task


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        read_only_fields = [
            'created_at',
            'modified_at',
            'completed_at',
            'is_completed',
        ]
        fields = [
            'id',
            'title',
            *read_only_fields,
        ]

    @staticmethod
    def get_is_completed(instance) -> bool:
        return instance.is_completed
