## TaskIO!

### How to Run?

There are two ways to use this application.

1. Using docker-compose file.

```
docker-compose up
```

Then open your browser and navigate to `localhost:8000`

In order to create an administrator user, use this:

```
docker-compose exec service python manage.py createsuperuser
```

2. Standalone

Install python requirements. (it's highly recommended to use virtual-env)

```
pip install -r requirements.txt
```

There are some environment variables that must be set before running the application.
Create `.env` file and set env variables in, then just run the following command.

```
python3 manage.py runserver localhost:8000
```

Open your browser and navigate to `localhost:8000`

In order to create an administrator user, use this:

```
python3 manage.py createsuperuser
```

### Environment Variables

1. SECRET_KEY (string)
2. DEBUG (boolean, optional, default=true)
3. ALLOWED_HOSTS (string, comma-separated)
4. DATABASE_PROVIDER (string, `sqlite3` or `postgres`)

Database Configuration:

1. Postgres

- POSTGRES_NAME (string)
- POSTGRES_USER (string)
- POSTGRES_PASSWORD (string)
- POSTGRES_HOST (string, optional, default=`localhost`)
- POSTGRES_PORT (int, optional, default=`5432`)

2. Sqlite3

- SQLITE_FILENAME (string, optional, default=`db.sqlite3`)

---

**Note**: This version is not ready for production. If you want to use this application on production, make sure `DEBUG`
env is false and use nginx or anything else to handle static files. You also may need to use uWSGI or Gunicorn.
